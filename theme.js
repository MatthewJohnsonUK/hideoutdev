$(document).ready(function(){

    /*

        Card Caption Normalising

     */
    var card_caption_tallest = 0;
    $("#cards .caption").each(function(){
        if ( $(this).innerHeight() > card_caption_tallest ){
            card_caption_tallest = $(this).innerHeight();
        }
    })
    .css('height', card_caption_tallest + 'px');

    /*

        Search Form Opening

     */
    var is_search_open = false;
    $("#searchOpen").click(function(){
        if ( is_search_open === false ) {
            $("#searchsection").addClass('open');
        }
    });

});