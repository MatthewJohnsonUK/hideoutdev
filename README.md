# Hideout Development Test

This repository is dedicated to the development test at The Hideout as defined by specification located at http://thehideout.co.uk/visuals/dev-test/

## Technologies

Code uses HTML5, CSS3, jQuery, Bootstrap and Sass - all of which are open source.

## Author

Matt Johnson, represented by Jess Maitland from Oscar Resourcing

## Web Root

This project can be found online at <http://odintech.uk/demo/hideout>